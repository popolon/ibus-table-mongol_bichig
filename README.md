Mongol Bichig, traditionnal mongolian script input table for IBus
=================================================================
Tradtionnal mongolian ("mongol bichig" meaning "mongolian writing" in Mongolian), also called "tsagaan tolgoj" (white head), is a writing script used by Mongolians since 12th century, when under Chingis khaan reign, Tatatunga adapted uyghur script to mongolian language. This is a custom method, adapted to a latin characters keyboard (for chinese mongolians and other mongolophons) and cyrillic mongolian keyboard (for Mongolia mongolians).

* (монгол бичиг/ᠮᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ)
* (en) An Input method for iBus to type traditional (uyghur) mongolian script.
* (fr) Une méthode de saisie pour iBus pour entrer de l'écriture mongole traditionnelle (dite ouïghoure).
* (zh) 传统蒙古文的输入法。 

Note: For cyrillic mongolian, you can use ibus table mongolian https://github.com/ochko/ibus-table-mongolian from Ochirkhuyag.L http://ochko.blogspot.com


Table of Contents
-----------------
* 1. Requirements
* 1.1. Software
* 1.2. Fonts
* 2. Installation
* 2.1 Requirements
* 2.2 Building the db file
* 2.3 Deploying
* 2.4 adding it in current methods in iBus.
* 3. Usage
* 3.1. Latin keys keyboard
* 3.2. Cyrillic keyboard
* 3.3. Other keyboards and going further
* 4. Improve it
* 5. Notes

1. Requirements
---------------
This software depend on Ibus and on a good text drawing engine. The mongolian writing depend on specific and rare fonts. See below (1.2 fonts and 2.1 Requirements) for instructions

1.1. Software
------------
You need to install ibus and ibus-table first (generally in seperates packages in linux distributions) as base software.

* Sources: https://github.com/ibus/ibus
* Wiki: https://github.com/ibus/ibus/wiki

To see the writing correctly, you need a software:
* Required: That is able to concacenate characters to make continuous strings and adapt them to the context, mainly, initial, middle or final character of the word. In my experience, most softwares but terminals
* Ideally: That write it vertically, as that's easier to read and write on a horse neck mounted screen this way. In my experience, only web browser (or some rare applications), with a page using a good stylesheet)

1.2 Fonts
---------
To see the writing you need mongolian fonts. The main free fonts used in linux and other free OS distributions are mongolfonts.com ones.
Android since version 5.0 (Lolipop) containt mongol bichig font by default.

2. Installation
---------------
2.1 Requirements

The following packages:
* iBus
* iBus-table

2.2 Building the db file

Go to the directory and type:

    ./compile.sh
    
2.3 Deploying

If you already built the .db, simply

    sudo ./deploy.sh
    
Else you can make everything in one step:

    sudo ./install.sh

2.4 adding it in current methods in iBus.

Go to the preferences box of ibus, by typing in a terminal:

    ibus-setup

Or by right clicking on the icon in your applets toolbar and selecting preferences/setup...

Then :
* Select the input methode tab
* click on the 3 vertical dots if you have lot of installed input methods
* click on Mongol/Mongolian item.
* Select the Mongol bichig (with this project icon).
* Press Add

You then can choose in the toolbar or by shortcut you defined (see the General tab).

3. Usage
--------
Type on your keyboard, the disposition relative to keyboards is more or less described in description.txt.

Search around the web about this kind of keyboards to have some ideas about it.


3.1 Latin keyboard
------------------
Mongolians from China (the main people to use everydays this writing) and non-ex-CCCP☭ countries use mainly latin character based keyboards

3.2 Cyrillic keyboard
---------------------
Mainly for Mongolia (Mongol Uls) and mongolians speaker/student from the ex-CCCP☭ countries.


3.3 Other keyboards and going further
-------------------------------------
The equivalent wraping need to be added to other non latin or cyrillic keyboards where text is purely phonetic, this include, arabian/persian, brahmi derivated scripts, greek, ethiopian, georgian, armenian, tamazigh and some other script from central asia, eastern europe, and north america

I would like to create similar methods for gögtürk, that is the oldest script found in Mongolia, still used today by historians.


4. Improve it
-------------
Feel free to improve it if you found errors. You can report errors in French, English, Chinese or Mongolian, I will try to understand it as far I know. Other people will be able to use it freely.

5. Notes
--------
     ψ   O                         ☽
     m         ___||___        O v,   
     |        /        \   ___[]//\
    _|_______|   []     |___//||\\______

Copyleft 2016 Popolon http://popolon.org

